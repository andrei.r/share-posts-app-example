<?php

namespace App;
use App\Database\Mongo\Connection;
use App\Post\Post;
use App\Post\PostRepository;
use App\Post\PostService;
use App\Decorators\HttpServiceJson;
use App\Decorators\HttpServiceJWTAuth;
use App\Auth\AuthService;

class SharePostsApp {

  // todo, move to env
  private const DBSTRING = 'mongodb://root:example@mongo:27017/';
  private const DBNAME ='test';
  
  private $db;

  function __construct() {
    $this->db = new Connection(self::DBSTRING,self::DBNAME);

    // register app repositories
    $this->postRepository = new PostRepository($this->db->selectCollection('posts'));

    // register app services
    $this->postService = new PostService($this->postRepository);
    $this->authService = new AuthService();

    // register authorization decorators 
    $this->postService = new HttpServiceJWTAuth($this->postService);

    // register json reponse decorators
    $this->postService = new HttpServiceJson($this->postService);

  }

  public function handleRequest() {
    header("Access-Control-Allow-Headers: *");
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
    
    unset($_SESSION['user_id']);
    unset($_SESSION['user_name']);

    

    $request_uri = $_SERVER['REQUEST_URI'];
    $request_method = $_SERVER['REQUEST_METHOD'];
    
    // get method from URI
    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);


    // static routes
    switch ($uri_parts[0]) {
      case '/posts':
        if($request_method == 'POST') {
          return $this->postService->post();
        }
        if($request_method == 'GET') {
          return $this->postService->get();
        }
        break;
      case '/login':
        header('Location: ' . AuthService::getLoginURL('login')); 
        break;
      case '/user':
        if($request_method == 'GET') {
          return $this->postService->get('user_name');
        }
        break;
      default:
        break;
    }

    // dynamic routes
    switch(1) {
      case preg_match("/^\/posts\/([a-zA-Z0-9]+)\/?$/", $request_uri, $postsURIMatches):
        $postid = $postsURIMatches[1];
        if($request_method == 'POST') {
          return $this->postService->put($postid);
        }
        if($request_method == 'GET') {
          return $this->postService->get($postid);
        }
        if($request_method == 'DELETE') {
          return $this->postService->delete($postid);
        }
        break;
      default:
        break;
    }

    // nothing matches
    return json_encode(['status' => 'error', 'error' => 'Method not found.']);
  }

}

