<?php

include_once 'SharePostsApp.php';
include_once 'HttpService.php';

include_once 'decorators/HttpServiceDecorator.php';
include_once 'decorators/HttpServiceAuthDecorator.php';
include_once 'decorators/HttpServiceJson.php';
include_once 'decorators/HttpServiceJWTAuth.php';

include_once 'database/mongo/Connection.php';
include_once 'database/mongo/RepositoryInterface.php';

include_once 'post/Post.php';
include_once 'post/PostRepository.php';
include_once 'post/PostService.php';

include_once 'auth/AuthService.php';


$app = new App\SharePostsApp();

return $app;

