<?php

namespace App\Decorators;
use App\Decorators\HttpServiceAuthDecorator;
use App\Auth\AuthService;

class HttpServiceJWTAuth extends HttpServiceAuthDecorator {

  public function get(string $query = null) {
    if($query == "user_name") {
      if ($this->validateAuth()) {
        return  ['user' => [ 'id' => $_SESSION['user_id'], 'name' => $_SESSION['user_name'] ]];
      } else {
        return null;
      }
    }
    return $this->service->get($query); 
  }

  public function post(array $data = null) {
    if ($this->validateAuth()) {
      return $this->service->post($data);
    }
    return $this->authErrorResponse();
  }

  public function put(string $id = null) {
    if ($this->validateAuth()) {
      return $this->service->put($id);
    }
    return $this->authErrorResponse();
  }

  public function delete(string $query = null) {
    if ($this->validateAuth()) {
      return $this->service->delete($query);
    }
    return $this->authErrorResponse();
  }

  public function login($state) {
    header('Location: ' . AuthService::getLoginURL($state));
  }


  protected function authErrorResponse() {
    http_response_code(401);
    $this->login();
    return false; 
  }

  protected function validateAuth(): bool {

    if (! preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
        header('HTTP/1.0 400 Bad Request');
        echo 'Token not found in request';
        exit;
    }

    $jwt = $matches[1];
    if (!$jwt) {
        // No token was able to be extracted from the authorization header
        header('HTTP/1.0 400 Bad Request');
        exit;
    }
    $authService = new AuthService();
    if(!$authService->validateAccessToken($jwt)) {
      return false;
    }
    return true;
  }
  
}