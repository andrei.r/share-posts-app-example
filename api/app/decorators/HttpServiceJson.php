<?php

namespace App\Decorators;
use App\Decorators\HttpServiceDecorator;

class HttpServiceJson extends HttpServiceDecorator {
  
  public function get(string $query = null): string {
    return $this->jsonEncodeResponse($this->service->get($query));
  }

  public function post(array $data = null): string {
    return $this->jsonEncodeResponse($this->service->post($data));
  }

  public function put(string $id = null): string {
    return $this->jsonEncodeResponse($this->service->put($id));
  }

  public function delete(string $query = null): string {
    return $this->jsonEncodeResponse($this->service->delete($query));
  }

  private function jsonErrorResponse(string $err = "Unknown error."): string {
    return json_encode(['status' => 'error', 'error' => $err]);
  }

  private function jsonEncodeResponse($data): string {
    header("Content-type: application/json");
    try {
      return json_encode($data);
    } catch (Exception $e) {
      return $this->jsonErrorResponse('Caught exception: ' .  $e->getMessage());
    }
  }
}