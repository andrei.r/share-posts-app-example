<?php

namespace App\Decorators;
use App\Decorators\HttpServiceDecorator;

abstract class HttpServiceAuthDecorator extends HttpServiceDecorator {
  abstract protected function validateAuth(): bool;
}
  