<?php

namespace App\Decorators;
use App\HttpService;

abstract class HttpServiceDecorator implements HttpService {

  protected HttpService $service;

  public function __construct(HttpService $service) {
    $this->service = $service;
  }

}