<?php 

namespace App\Auth;
use \Lcobucci\JWT\Signer\Hmac\Sha256;
use \Lcobucci\JWT\Configuration;
use \Lcobucci\JWT\Signer\Key\InMemory;
use \Lcobucci\JWT\Validation\RequiredConstraintsViolated;
use \Lcobucci\JWT\Validation\Constraint\IssuedBy;
use \Lcobucci\JWT\Validation\Constraint\SignedWith;



class AuthService {

  private const JWT_SECRET = "14k32nj4n321";
  private const AUTH_ISSUER = "authserver";
  private const CLIENT_ID = "client1";
  private const CLIENT_SECRET = "client1secret";

  private Configuration $config;

  public function __construct() {
    
    $this->config = Configuration::forSymmetricSigner(
        new Sha256(),
        InMemory::plainText(self::JWT_SECRET)
    );
  }


  public function validateAccessToken($code): bool {
    $token = $this->config->parser()->parse($code);

    if($token->isExpired(new \DateTime('NOW'))) {
      return false;
    }

    if(!$token->hasBeenIssuedBy(self::AUTH_ISSUER)) {
      return false;
    }

    $_SESSION['user_id'] = $token->claims()->get('sub');
    $_SESSION['user_name'] =  $token->claims()->get('usn');
     
    return true;
  }

  public static function getLoginURL($state): string {
    return $_ENV['LOGIN_URL'] . '?response_type=code&client_id=' . self::CLIENT_ID . '&redirect_uri=' . $_ENV['BASE_URL'] . '&scope=posts&state=' . $state;
  }

}