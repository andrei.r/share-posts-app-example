<?php 

namespace App\Post;

class Post implements \MongoDB\BSON\Persistable {

  private \MongoDB\BSON\ObjectId $id;
  private string $title;
  private string $description;
  private string $author_id;
  private string $author_name;
  private string $publish_date;

  public function __construct(
    string $title,
    string $description,
    string $author_id,
    string $author_name
  ) { 
    $this->id = new \MongoDB\BSON\ObjectId;
    $this->title = $title;
    $this->description = $description;
    $this->author_id = $author_id;
    $this->author_name = $author_name;
    $this->publish_date = new \MongoDB\BSON\UTCDateTime;
  }

  public function isPublished(): bool {
    return is_null($this->published_date);
  }
  
  public function bsonSerialize() {
    return [
      '_id' => $this->id,
      'title' => $this->title,
      'description' => $this->description,
      'author_id' => $this->author_id,
      'author_name' => $this->author_name,
      'publish_date' => $this->publish_date
    ];
  }

  public function bsonUnserialize(array $data) {
    $this->id = $data['_id'];
    $this->title = $data['title'];
    $this->description = $data['description'];
    $this->author_id = $data['author_id'];
    $this->author_name = $data['author_name'];
    $this->publish_date = $data['publish_date'];
  }

}
