<?php 

namespace App\Post;
use App\Post\Post;
use App\Post\PostRepository;
use App\HttpService;

class PostService implements HttpService {

  private PostRepository $repository;

  public function __construct(PostRepository $repository) {
    $this->repository = $repository;
  }

  public function post(array $data = null) {
    $title = $this->validatePostField($_POST["title"]);
    $description = $this->validatePostField($_POST["description"]);
    $author_id =  $this->validatePostField($_POST["author_id"]);
    $author_name = $this->validatePostField($_POST["author_name"]);

    if(empty($title) || empty($description)) {
      return [ "status" => "false"];
    }
    $this->repository->save(
      new Post(
        $title,
        $description,
        $author_id,
        $author_name
      )
    );
    return ["status" => "ok"];
  }

  public function get(string $id = null) {
    if(!is_null($id)) {
      return $this->repository->findOneById($id)->bsonSerialize();
    }
    return $this->repository->find();
  }

  public function put(string $id = null) {
    $title = $this->validatePostField($_POST["title"]);
    $description = $this->validatePostField($_POST["description"]);
    $postid = $this->validatePostField($id);

    if(empty($title) || empty($description) || empty($postid)) {
      return [ "status" => "false"];
    }

    if($this->repository->update($postid, [ 'title' => $title, 'description' => $description])) {
      return [ "status" => "ok" ];
    }

    return [ "status" => "false" ];
  }

  public function delete(string $id = null) {
    $postid = $this->validatePostField($id);
    if(empty($postid)) {
      return [ "status" => "false"];
    }

    if($this->repository->delete($postid)) {
      return [ "status" => "ok" ];
    }

    return [ "status" => "false" ];
  }

  private function validatePostField($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
}