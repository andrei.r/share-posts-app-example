<?php 

namespace App\Post;
use App\Post\Post;


class PostRepository implements \App\Database\Mongo\RepositoryInterface {
  private $store;

  public function __construct($store) { 
    $this->store = $store;
    $this->store->createIndex(['publish_date' => -1]);
  }

  public function findOneById(string $id): Post {
    $arrayData = $this->store->findOne( [ '_id' => new \MongoDB\BSON\ObjectId("$id") ] );
    if (is_null($arrayData)) {
      return null;
    }
    return $arrayData;
  }

  public function findOne(array $filter): Post {

  }

  public function find(): array {
    $docs = $this->store->find([], [ 'sort' => [ 'publish_date' => -1 ]]);
    $posts = array();
    foreach ($docs as $doc) {
      array_push($posts, $doc->bsonSerialize());
    };
    return $posts;
  }

  public function save(\MongoDB\BSON\Persistable $post) {
    $this->store->insertOne($post);
  }


  public function update(string $id, array $data) {
    $updateResult = $this->store->updateOne( 
      [ '_id' =>  new \MongoDB\BSON\ObjectId("$id") ],
      [ '$set' => [ 'title' => $data['title'], 'description' => $data['description'] ]]
    );
    if($updateResult->getModifiedCount() == 1) {
      return true;
    }
    return false;
  }

  public function delete(string $id) {
    $deleteResult = $this->store->deleteOne( 
      [ '_id' =>  new \MongoDB\BSON\ObjectId("$id") ]
    );
    if($deleteResult->getDeletedCount() == 1) {
      return true;
    }
    return false;
  }
}