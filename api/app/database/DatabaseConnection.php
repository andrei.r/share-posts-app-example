<?php 

namespace App\Database;

interface DatabaseConnection {
  public function selectDatabase(string $dbname);
}
