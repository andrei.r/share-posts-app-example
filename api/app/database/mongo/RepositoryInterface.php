<?php 

namespace App\Database\Mongo;

interface RepositoryInterface {
  public function findOneByID(string $id): \MongoDB\BSON\Persistable;
  public function findOne(array $filter): \MongoDB\BSON\Persistable;
  public function find():array;
  public function save(\MongoDB\BSON\Persistable $entity);
  public function update(string $id, array $data);
  public function delete(string $id);
}