<?php 
namespace App\Database\Mongo;

class Connection {
  private $db;

  function __construct(string $dbstring, string $dbname) {
    $client = new \MongoDB\Client($dbstring);
    $this->db = $client->selectDatabase($dbname); 
  }

  public function selectCollection(string $collection) {
    return $this->db->selectCollection($collection);
  }
}
