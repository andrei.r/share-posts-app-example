<?php

namespace App;

interface HttpService {
  public function post(array $data = null);
  public function put(string $id = null);
  public function get(string $query = null);
  public function delete(string $query = null);
}