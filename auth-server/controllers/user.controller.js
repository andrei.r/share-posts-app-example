const { body, validationResult } = require('express-validator')
const createHttpError = require('http-errors')

const { addUser, getUser, checkEmailAndPassword } = require('../services/user.services')

const findOneUserByID = async (req, res, next) => {
  const { id } = req.params
  try {
    res.send(await getUser(id))
  } catch (err) {
    return next(err)
  }
}

const login = [
  body('email').trim().escape(),
  body('password').trim().escape(),
  async (req, res, next) =>{
    const { email, password } = req.body
    try {
      let user = await checkEmailAndPassword(email, password);
      if (!user) {
        return next(createHttpError(401, 'Invalid password and email combination'))
      }
      res.locals.user = user;
    } catch (err) {
      return next(err)
    }
    return next()
  }
]

const register = [
  body('firstname').trim().escape(),
  body('lastname').trim().escape(),
  body('email').trim().escape(),
  body('password').trim().escape(),
  body('password_check').trim().escape(),
  async (req, res, next) => {
    const { firstname, lastname, email, password, password_check } = req.body
    if(password != password_check) {
      return next(createHttpError(400, "Password check failed"))
    }
    try {
      res.locals.user =  await addUser(firstname, lastname, email, password) 
    } catch(err) {
      return next(err)
    }
    return next()
  }
]

module.exports = {
  findOneUserByID,
  register,
  login
}