const createHttpError = require("http-errors")

const { getAuthOptions, generateAuthCode, getTokenOptions, generateAccessToken, validateAuthCode } = require('../services/auth.services')

const validateAuthOptions = (req, res, next) => {
  try {
    let optsHolder = req.method == "GET" ? req.query : req.body;
    const options = getAuthOptions(optsHolder)
    for (const opt in options) {
      res.locals[opt] = options[opt]
    }
  } catch (err) {
    return next(err);
  }
  return next()
}

const grantAuthCode = async (req, res, next) => {
  let redirectURI = res.locals.redirect_uri
  if (res.locals.response_type == "code") {
    let auth_code = await generateAuthCode(res.locals.user, res.locals.client_id)
    redirectURI += `?code=${auth_code}&state=${res.locals.state}`
  } 
  res.redirect(redirectURI)
  return next()
}

const validateTokenOptions = async (req, res, next) => {
  try {
    const options = getTokenOptions(req.body)
    for (const opt in options) {
      res.locals[opt] = options[opt]
    }
  } catch (err) {
    return next(err);
  }
}


const grantAccessToken = (req, res, next) => {
  try {
    const decoded = validateAuthCode(res.locals.code)
    res.send(generateAccessToken(decoded, res.locals.client_id))
  } catch(err) {
    return next(err)
  }
}

module.exports = {
  validateAuthOptions,
  grantAuthCode,
  validateTokenOptions,
  grantAccessToken
}