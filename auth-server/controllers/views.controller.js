const renderLogin = function (req, res, next) {
  res.render('login', { title: 'Login for SharePosts' });
}

const renderRegister = function (req, res, next) {
  res.render('register', { title: 'Register for SharePosts' });
}


module.exports = {
  renderLogin,
  renderRegister
}