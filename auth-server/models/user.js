var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema(
  {
    firstname: { type: String, required: true, maxlength: 100 },
    lastname: { type: String, required: true, maxlength: 100 },
    email: {
      type: String,
      unique: true,
      required: true, 
      validate: {
        validator: function(value) {
          return /^[a-zA-Z]+?[a-zA-Z0-9]*@.+/.test(value);
        },
        message: props => `${props.value} is not a valid email.`
      }
    },
    password: { type: String, required: true }
  },
  {
    autoCreate: true
  }
);

UserSchema
  .virtual('name')
  .get(function () {
    return this.firstname + ' ' + this.lastname;
  });

module.exports = mongoose.model('User', UserSchema);