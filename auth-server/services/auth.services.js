const createHttpError = require("http-errors");
const jwt = require('jsonwebtoken');

// TODO: move to external store
const clients = ['client1']
const clients_secrets = ['client1secret']
const auth_server_name = "authserver"
const jwt_secret = process.env.JWT_SECRET

const getAuthOptions = ({ response_type, client_id, redirect_uri, scope, state }) => {

  if (response_type != "code") {
    throw createHttpError(405, `Response type ${response_type} is not implemented.`);
  }

  if (!clients.includes(client_id)) {
    throw createHttpError(401);
  }

  if (!redirect_uri || !scope || !state) {
    throw createHttpError(400);
  }

  return {
    response_type,
    client_id,
    redirect_uri,
    scope,
    state
  }
}

const getTokenOptions = ({ code, grant_type, client_id, client_secret }) => {

  if (grant_type != "authorization_code") {
    throw createHttpError(405, `Grant type ${grant_type} is not implemented.`)
  }

  if (!clients.includes(client_id) || !clients_secrets(client_secret)) {
    throw createHttpError(401);
  }

  return {
    code,
    grant_type,
    client_id,
    client_secret
  }
}

const validateAuthCode = (code, client_id) => {
  try {
    var decoded = jwt.verify(code, secret, {
      issuer: auth_server_name,
      audience: client_id
    })
    return true
  } catch (err) {
    throw createHttpError(403, err.message)
  }
}

const generateAccessToken = (authCode, client_id) => {
  const access_token = {
    sub: authCode.sub,
    iss: auth_server_name,
    aud: client_id,
    usn: authCode.usn,
    iat: Date.now(),
  };
  return jwt.sign(access_token, jwt_secret, {
    expiresIn: '3600s'
  })
}

const generateAuthCode = (user, client_id) => {
  const auth_token = {
    sub: user.id,
    iss: auth_server_name,
    aud: client_id,
    usn: user.name,
    iat: Date.now(),
  };
  return jwt.sign(auth_token, jwt_secret, {
    expiresIn: '60s'
  })
}


module.exports = {
  getAuthOptions,
  generateAuthCode,
  getTokenOptions,
  validateAuthCode,
  generateAccessToken
}