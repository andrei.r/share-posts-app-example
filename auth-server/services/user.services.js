
const User = require('../models/user')
const bcrypt = require('bcrypt')

const addUser = async (firstname, lastname, email, password) => {
  const saltRounds = 10;
  const hashedPass = await bcrypt.hash(password, saltRounds)

  return new Promise((resolve, reject) => {
    const user = new User({
      firstname,
      lastname,
      email,
      password: hashedPass
    })
    user.save(function (err) {
      if (err) { reject(err); }
      resolve(user)
    })
  })
}

const checkEmailAndPassword = async (email, password) => {
  try {
    const user = await User.findOne({ email }).exec()
    if(bcrypt.compare(password, user.password)) {
      return user
    }
  } catch(e) {
    throw e;
  }
  return null
}

const getUser = async (id) => {
  return await User.findById(id).exec();
}

module.exports = {
  addUser,
  getUser,
  checkEmailAndPassword
}