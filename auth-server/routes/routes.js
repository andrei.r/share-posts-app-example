var express = require('express')
var router = express.Router()

const auth_controller = require('../controllers/auth.controller')
const user_controller = require('../controllers/user.controller')

router.get('/authorize', [
  auth_controller.validateAuthOptions,
  (req, res, next) => {
    res.render('login', {
      title: 'Login for SharePosts',
      response_type: res.locals.response_type,
      client_id: res.locals.client_id,
      redirect_uri: res.locals.redirect_uri,
      scope: res.locals.scope,
      state: res.locals.state
    })
  }
])

router.get('/register', [
  auth_controller.validateAuthOptions,
  (req, res, next) => {
    res.render('register', {
      title: 'Register for SharePosts',
      response_type: res.locals.response_type,
      client_id: res.locals.client_id,
      redirect_uri: res.locals.redirect_uri,
      scope: res.locals.scope,
      state: res.locals.state
    })
  }
])

router.get('/users/:id', user_controller.findOneUserByID)

router.post('/token', [
  auth_controller.validateTokenOptions,
  auth_controller.grantAccessToken
])

router.post('/users/create', [
  auth_controller.validateAuthOptions,
  user_controller.register,
  auth_controller.grantAuthCode
])

router.post('/users/login', [
  auth_controller.validateAuthOptions,
  user_controller.login,
  auth_controller.grantAuthCode
])


module.exports = router;
