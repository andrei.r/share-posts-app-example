# Share Posts App Example

## Description
Example social network style app built using PHP, Nodejs and MongoDB. 

Use cases:
- As a visitor user I can view all posts. 
- As a visitor user I can register for an account using my name, email and a password. 
- As a visitor user I am not allowed to create multiple accounts using the same email address.
- As a visitor user I can login in my account using my email and password.
- As a logged-in user I can share a new post by filling in the required fields title and description.
- As a logged-in user I can delete a post, if I was the user that published it.
- As a logged-in user I can update the title and description of a post, I was the user that published it.

https://gitlab.com/andrei.r/share-posts-app-example/-/blob/main/SharePost-sequence-diagram.png

## Installation

### Using docker

Prerequisites: 
- Install `docker` & `docker-compose`
- Create `.env` file using the template `.env.template`

How to start:
- Use `docker-compose.yml` file from top folder to bring up the stack.
- Navigate to http://localhost/ in your browser.

Images build required: 
- ```docker build -t shareposts/auth:php-fpm ./api```
- ```docker build -f ./api/Dockerfile-nginx -t shareposts/api:dev ./api```
- ```docker build -t shareposts/auth:dev ./auth-server```
- ```docker build -t shareposts/webapp:dev ./webapp```

### Locally
Prerequisites
- `Mongodb` database
- `Node.js` >= 14
- Create `.env` file using the template `.env.template`

How to start:
- Start API service: 
  -  Navigate to `api` folder and install dependencies:
      - composer require mongodb/mongodb
      - composer require lcobucci/jwt
  -  Start local server and serve `public/` folder
- Start Auth service:
  - Navigate to `auth-server` folder and install dependencies using `npm install`
  - Run service using `npm run start`
- Start local web app:
  - Navigate to `webapp` folder and install dependencies using `npm install`
  - Run app using `node index.js`
  
