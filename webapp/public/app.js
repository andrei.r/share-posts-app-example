(function (doc) {
  // initialize data store
  var store = {
    user: null,
    error: null,
    title: null,
    description: null,
    posts: [],
  }

  const postComp = Vue.component('postcomp', {
    props: ['user', 'id', 'title', 'description', 'author_name', 'author_id', 'publish_date'],
    data: function() {
      return {
        editMode: false
      }
    },
    template: `<li v-bind:id="postId">
      <div v-if="editMode == false">
        <p>{{ title }}</p>
        <p>{{ description }}</p>
      </div>
      <form v-if="editMode == true" @submit="editFormSubmit" method="POST" action="#">
        <input type="text" v-model="title" name="title" />
        <input type="text" v-model="description"  name="description"  />
        <button type="submit">Edit!</button>
      </form>
      <p>{{ author_name }}, {{ postPublishDate }}</p>
      <p v-if="user !== null && user.id == author_id "><a v-on:click="editPost" href="#">Edit</a> || <a v-on:click="deletePost" href="#">Delete</a></p>
      
    </li>`,
    methods: {
      editPost: function (e) {
        e.preventDefault()
        console.log('edit', this.postId)
        this.editMode = !this.editMode
      },
      deletePost: function (e) {
        e.preventDefault()
        postData('http://localhost:8080/posts/' + this.postId, {}, 'DELETE')
        .then(async () => {
          store.posts = await retrievePosts()
        })
      },
      editFormSubmit: function(e) {
        e.preventDefault()
        console.log(this.title, this.description, this.postId)
        postData('http://localhost:8080/posts/' + this.postId, {
          title: this.title,
          description: this.description
        }).then(async () => {
          store.posts = await retrievePosts()
          this.editMode = false
        })
      }
    },
    computed: {
      postPublishDate: function() {
        return new Date(parseInt(this.publish_date)).toLocaleDateString()
      },
      postId: function() {
        return this.id['$oid']
      }
    }
  })

  // create a new Vue app
  const app = new Vue({
    el: '#app',
    data: store,
    methods: {
      checkForm: function (e) {
        e.preventDefault();
        postData('http://localhost:8080/posts', {
          title: this.title,
          description: this.description,
          author_id: store.user.id,
          author_name: store.user.name
        }).then(async () => {
          store.posts = await retrievePosts()
        })
      },
      logout: function (e) {
        e.preventDefault()
        store.user = null
        localStorage.removeItem('jwt')
      },

    },
    mounted: () => {
      retrieveUser().then((value) => {
        store.user = value
      })

      retrievePosts().then((value) => {
        store.posts = value
        console.log(value)
      })
    }
  })

  var searchParams = new URLSearchParams(window.location.search);
  if (searchParams.get('code')) {
    window.localStorage.setItem('jwt', searchParams.get('code'));
  }


  // retrieve posts
  async function retrievePosts() {
    let posts = await getData('http://localhost:8080/posts')
    if (!posts) {
      return []
    }
    return posts
  }

  // retrieve logged-in user
  async function retrieveUser() {
    return (await getData('http://localhost:8080/user'))['user']
  }

  // get data from API
  async function getData(url = '') {
    try {
      var response = await fetch(url, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
      });
    } catch (e) {
      console.error('There has been a problem with the fetch operation:', e);
      return;
    }
    let result = await response.json();
    if (result.status === 'error') {
      console.error('Server error ', result.error);
      return null
    }

    return result;
  }

  // post data to API
  async function postData(url = '', data = {}, method = 'POST') {
    try {
      console.log(data)
      var response = await fetch(url, {
        method: method,
        cache: 'no-cache',
        mode: "cors",
        credentials: "same-origin",
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: new URLSearchParams(data)
      });
    } catch (e) {
      console.error('There has been a problem with the fetch operation:', e);
      return;
    }
    let result = await response.json();
    if (result.status === 'error') {
      console.error('Server error ', result.error);
      return null
    }

    return result;
  }


})()
